export const errors = {
    ERROR_NO_VALUE: "You didn't write any value",
    ERROR_NOT_NUMBER: "You must enter a number",
    ERROR_BOOLEAN: "Write a number, not boolean",
    ERROR_POSITIVE_NUMBER: "Enter positive number",
    ERROR_FINITE_NUMBER: "Enter finite number",
    ERROR_WHOLE_NUMBER: "Enter whole number",
    ERROR_ONLY_NUMBERS_ALLOWED: "Only numbers allowed",
    ERROR_SCIENTIFIC_NOTATION: "Scientific notation not allowed",
};
