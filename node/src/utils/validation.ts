import {errors} from './errorConstants';

export function isInputValid(input: string): string {
    let result = "";

    if (input.length === 0) {
        result = errors.ERROR_NO_VALUE;

    } else if (isNaN(Number(input))) {
        result = errors.ERROR_NOT_NUMBER;

    } else if (typeof input === "boolean") {
        result = errors.ERROR_BOOLEAN;

    } else if (parseInt(input) <= 0) {
        result = errors.ERROR_POSITIVE_NUMBER;

    } else if (!isFinite(Number(input))) {
        result = errors.ERROR_FINITE_NUMBER;

    } else if (parseInt(input) % 1 !== 0) {
        result = errors.ERROR_WHOLE_NUMBER;

    } else if (input.indexOf(".") > -1) {
        result = errors.ERROR_ONLY_NUMBERS_ALLOWED;

    } else if (isScientificNotation(input)) {
        result = errors.ERROR_SCIENTIFIC_NOTATION;
    }

    return result;
}

function isScientificNotation(number: string): boolean {
    const scientificNotationPattern = /^[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)$/;
    return scientificNotationPattern.test(number);
}
