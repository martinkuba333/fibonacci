//1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987
export function getNthFibonnacciNum(input: number): number | string {
    let result;
    if (input < 3) {
        result = 1;

    } else if (input > 10000) {
        result = "Infinity";

    } else {
        let first = 1;
        let sec = 1;

        for (let i = 2; i < input; i++) {
            const temp = first + sec;
            first = sec;
            sec = temp;
        }
        result = sec;
    }
    return result;
}
