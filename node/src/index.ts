import { getNthFibonnacciNum } from './service/calculation';
import { isInputValid } from './utils/validation';
import { Request, Response } from 'express';

const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors({
    origin: function (origin: string, callback: Function) {
        if (!origin || origin.startsWith('http://localhost')) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    }
}));

app.get('/', (req: Request, res: Response) => {
    let result = "";
    let errorMsg = "Input parameter missing";

    if (typeof req.query.input !== "undefined") {
        const input: string = req.query.input as string;
        errorMsg = isInputValid(input);

        if (errorMsg.length === 0) {
            result = getNthFibonnacciNum(parseInt(input)).toString();
        }
    }

    res.send({
        result: result,
        errorMsg: errorMsg,
    });
});

app.get('/test', (req: Request, res: Response) => {
    res.send('Hello, World!');
});

const port = process.env.PORT || 3001;

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
