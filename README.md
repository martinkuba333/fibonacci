# Fibonacci

## Getting started
### Install npm globally if missing in your system
- npm install -g npm@latest

### React part
- cd react
- npm install
- npm run start

### Node.js part
- cd node
- npm install
- npm run start
