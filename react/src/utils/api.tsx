import axios from 'axios';

interface FetchDataResponse {
    result: string;
    errorMsg: string;
}

export const fetchData = async (inputValue: string): Promise<FetchDataResponse> => {
    const fetchDataResponse: FetchDataResponse = {result: '', errorMsg: ''}
    try {
        const url = process.env.REACT_APP_FETCH_DATA_URL || "http://localhost:3001";
        const response = await axios.get(`${url}?input=${inputValue}`);
        if (response.status === 200) {
            if (response.data.result) {
                fetchDataResponse.result = response.data.result;

            } else if (response.data.errorMsg) {
                fetchDataResponse.errorMsg = response.data.errorMsg;
            }
        } else {
            fetchDataResponse.errorMsg = `Something wrong: ${response.status}`;
        }
    } catch (error: any) {
        fetchDataResponse.errorMsg = error.message;
    }
    return fetchDataResponse;
};
