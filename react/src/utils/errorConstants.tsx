export const ERROR_NO_VALUE = "You didn't write any value";
export const ERROR_NOT_NUMBER = "You must enter a number";
export const ERROR_POSITIVE_NUMBER = "Enter positive number";
export const ERROR_FINITE_NUMBER = "Enter finite number";
export const ERROR_WHOLE_NUMBER = "Enter whole number";
export const ERROR_ONLY_NUMBERS_ALLOWED = "Only numbers allowed";
