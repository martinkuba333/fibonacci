import React, {useState} from 'react';
import {Form, Input, Submit, Error, Result, Title} from "./App.styles";
import {
    ERROR_FINITE_NUMBER, ERROR_NO_VALUE, ERROR_NOT_NUMBER,
    ERROR_ONLY_NUMBERS_ALLOWED,
    ERROR_POSITIVE_NUMBER,
    ERROR_WHOLE_NUMBER
} from "../utils/errorConstants";
import {fetchData} from "../utils/api";

function App() {
    const [inputValue, setInputValue] = useState<string>('');
    const [errorMsg, setErrorMsg] = useState<string>('');
    const [result, setResult] = useState<string>('');

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(event.target.value)
    }

    const handleKeyPress = (event: React.KeyboardEvent) => {
        if (event.key === "Enter") {
            handleSubmit();
        }
    }

    const handleSubmit = () => {
        const validationError = validateInput();
        if (validationError === "") {
            handleFetchData();
        } else {
            setErrorMsg(validationError)
            setResult("");
        }
    }

    const validateInput = (): string => {
        let result = "";
        //uncomment for skip frontend validation
        //return result;
        const input = inputValue.toString();

        if (input.length === 0) {
            result = ERROR_NO_VALUE;

        } else if (isNaN(Number(input))) {
            result = ERROR_NOT_NUMBER;

        } else if (parseInt(input) <= 0) {
            result = ERROR_POSITIVE_NUMBER;

        } else if (!isFinite(Number(input))) {
            result = ERROR_FINITE_NUMBER;

        } else if (parseInt(input) % 1 !== 0) {
            result = ERROR_WHOLE_NUMBER;

        } else if (input.indexOf(".") > -1) {
            result = ERROR_ONLY_NUMBERS_ALLOWED;
        }

        return result;
    }

    const handleFetchData = async (): Promise<void> => {
        const {result, errorMsg} = await fetchData(inputValue);
        setResult(result);
        setErrorMsg(errorMsg);
    }

    return (
        <>
            <Form>
                <Title>Get nth number of Fibonacci sequence</Title>
                <Input
                    type={"text"}
                    value={inputValue}
                    onChange={handleInputChange}
                    onKeyPress={handleKeyPress}
                    placeholder={"Write number"}
                />
                <Submit
                    onClick={handleSubmit}
                >Send</Submit>
                {errorMsg.length > 0 ?
                    <Error>{errorMsg}</Error> : null
                }
                {result.length > 0 ?
                    <Result>Result: {result}</Result> : null
                }
            </Form>
        </>
    );
}

export default App;
