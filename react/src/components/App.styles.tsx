import React from 'react';
import styled from 'styled-components';
import exp from "constants";

export const Form = styled.div`
  width: 80%;
  margin: 20% auto;
`;

export const Input = styled.input`
  width: 50%;
  margin: auto;
  display: flex;
  justify-content: center;
  border: 3px solid black;
  font-size: 25px;
  padding: 5px;
`;

export const Submit = styled.div`
  color: white;
  background: lightgreen;
  text-transform: uppercase;
  font-weight: bold;
  padding: 20px;
  border-radius: 5px;
  font-size: 25px;
  width: fit-content;
  margin: 20px auto;
  user-select: none;

  &:hover {
    cursor: pointer;
    background: green;
    animation: all 2s linear;
  }
`;

export const Error = styled.div`
  background: red;
  color: white;
  width: fit-content;
  margin: auto;
  user-select: none;
  padding: 20px;
  border-radius: 5px;
  font-size: 25px;
  font-weight: bold;
`;

export const Result = styled.div`
  border: 5px solid green;
  text-align: center;
  padding: 5px;
  width: fit-content;
  margin: auto;
  font-size: 25px;
  font-weight: bold;
`;

export const Title = styled.h2`
  text-align: center;
`
